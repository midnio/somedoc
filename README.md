# somedoc
# THIS PROJECT NOT DEVOLOPING ANYMORE
Teaching what i learn(ed).
Everyday new one =) The docs
will be so hard, ***no details***.
# Python

### Printing text

`print(anything)`

### Data types

Numbers: int (like 1, 2, 900_000)
Texts (Strings): str (like "Hello" or 'Hello')
Float Numbers: float (like 0.1, .5)
Lists (Arrays): list (Like [2, 7, 8])
Tuples: tuple (Like (2, 7, 8))
Booleans: bool (like True or False)
Nothing: None

### Defining variables

`a = 2`

### Printing Varibales

```
a = 2
print(a)
```
### Comments

`# Comment something`

## Loops

### For loop

```
for a in [2, 7, 8]:
  print(a)
````

### While loop

```
i = 0
while i < 5:
  print(i)
  i += 1
```
